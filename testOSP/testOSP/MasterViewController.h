//
//  MasterViewController.h
//  testOSP
//
//  Created by Joel on 18/01/17.
//  Copyright © 2017 Joel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@class DetailViewController;

//@interface MasterViewController : UITableViewController <NSFetchedResultsControllerDelegate>
@interface MasterViewController : UITextField <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) DetailViewController *detailViewController;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;


@end

@interface Texto1  : UITextField <NSFetchedResultsControllerDelegate>
@end
@interface Texto2 : UITextField <NSFetchedResultsControllerDelegate>
@end