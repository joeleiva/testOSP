//
//  DetailViewController.h
//  testOSP
//
//  Created by Joel on 18/01/17.
//  Copyright © 2017 Joel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

